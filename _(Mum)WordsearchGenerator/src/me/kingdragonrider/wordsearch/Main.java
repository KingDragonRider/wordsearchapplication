package me.kingdragonrider.wordsearch;

import java.awt.Cursor;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.regex.Pattern;

import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PGraphics;
import processing.core.PImage;
import processing.event.MouseEvent;

//import processing.sound.*;

public class Main extends PApplet
{
	static int wordBankTextSize, wordBankBoxWidth, wordBankBoxHeight;

	private Set<Integer> keysPressed = new HashSet<>();

	// private int borderColor = color(0, 51, 102), mainColor = color(255, 255,
	// 0), headerColor = color(51, 153, 204), textColor = color(0, 51, 102),
	// buttonBorder = color(51,
	// 51, 102), correctColor = color(0, 153, 0), wrongColor = color(204, 0, 0),
	// completedColor = color(255, 102, 0);
	private PFont arial, lSans;
	private PImage icon, check, eraserImage, textBackground, deleteButton, exportWordsearch, newWordsearch, showCharacters, tintToggle, copyWords, pasteWords, errorIcon;
	private LetterCell[][] cells;
	private int insideX, insideY, insideWidth, insideHeight;
	private Dialog activeDialog;
	private int mouseXStart, mouseYStart;
	private long lastMousePress = -1;
	private long cursorFlashTimeout = 1100;
	private WordBoard wordBoard;
	private String fileLocation;
	private int tintColour;
	private List<String> words = new ArrayList<>();
	private Main main;
	private List<IconButton> buttons = new ArrayList<>();
	private boolean adjustedPosition = false;
	private String[] tips = { "Dragging a word outside the wordsearch will remove it",
			"Shift + clicking the 'New Wordsearch' button will clear the existing wordsearch instead of creating a new one",
			"Clicking the 'Show Characters' button will regenerate the random letters",
			"If you hover over a letter that is used by multiple words, use the mouse wheel to select the word you want",
			"You can move the 'New Wordsearch' dialog by dragging the blue title bar at the top",
			"The words in the Word Bank will display an error when they cannot be placed",
			"Copying the words (with Ctrl + C or the 'Copy Words' button) will also save the locations of words already placed",
			"When you have a word selected, you can change its orientation by using the mouse wheel", "Click me to cycle through all the tips",
			"Pressing the Return key will automatically select the 'Add Word' input box", "Dragging the right mouse button over words will remove them" };
	private int tipCounter;
	private int offsetCounter = (int) (Math.random() * tips.length), tipFrameCount = 0;
	private int framesPerTip = 60 * 12, interval = 60 * 2, fadeInOutTime = 30 * 3;
	private Process process;

	public enum TextInputType
	{
		NORMAL, NUMERICAL("[0-9]"), NUMERICAL_DECIMAL, LETTERS("[a-zA-Z]"), TITLE("[\\u0000-\\u007F]+"), SPELLINGS("[a-zA-Z\\'-]"), ALPHANUMERICAL;

		public String regEx;
		private Pattern pattern;

		TextInputType(String regEx)
		{
			this.regEx = regEx;
			if (regEx != null)
			{
				pattern = Pattern.compile(regEx);
			}
		}

		TextInputType()
		{
			this(null);
		}

		public boolean isValid(String character)
		{
			if (pattern == null)
				return true;
			return pattern.matcher(character).find();
		}

		public boolean isValid(Character character)
		{
			return this.isValid(Character.toString(character));
		}
	}

	public enum WordError
	{
		NONE(null), TOO_LONG("This word is too long."), INELIGIBLE("No available spaces left.");

		String errorMessage;

		WordError(String errorMessage)
		{
			this.errorMessage = errorMessage;
		}
	}

	public enum ButtonResponse
	{
		CANCEL, CREATE;
	}

	public enum ButtonState
	{
		PRESSED, SELECTED, MOUSE_OVER, INACTIVE, ACTIVE;
	}

	public enum CellEligibility
	{
		PREFERED, ELIGIBLE, INELIGIBLE;
	}

	public static void main(String[] args)
	{
		PApplet.main("me.kingdragonrider.wordsearch.Main");
	}

	public void settings()
	{
		size(1236, 700);
	}

	public void mouseMoved()
	{
		updateCursor();
	}

	public void updateCursor()
	{
		boolean mouseOver = false;
		int cursorType = Cursor.DEFAULT_CURSOR;
		if (activeDialog != null)
		{
			for (DialogObject tI : activeDialog.dialogObjects)
				if (!mouseOver && tI.mouseOver() && tI instanceof TextInput)
					cursorType = Cursor.TEXT_CURSOR;
		}
		else if (wordBoard != null)
		{
			if (!(mousePressed && mouseButton == RIGHT))
			{
				if (wordBoard.nameInputBox.mouseOver() || wordBoard.wordBank.addNewWord.mouseOver())
					cursorType = Cursor.TEXT_CURSOR;
				else if (wordBoard.wordsUnderMouse.size() > 0)
					cursorType = Cursor.MOVE_CURSOR;
			}
			else
			{
				if (wordBoard.mouseOver())
				{
					cursorType = Cursor.CUSTOM_CURSOR;
					cursor(eraserImage, 2, 2);
				}
			}
		}

		if (cursorType != Cursor.CUSTOM_CURSOR)
			cursor(cursorType);
	}

	public void setup()
	{
		main = this;
		frameRate(60);
		// if(!adjustedPosition)
		// {
		surface.setLocation(displayWidth / 2 - width / 2, 0);
		// surface.

		// PSurfaceAWT awtSurface = (PSurfaceAWT)surface;
		// PSurfaceAWT.SmoothCanvas smoothCanvas =
		// (PSurfaceAWT.SmoothCanvas)awtSurface.getNative();
		// smoothCanvas.getFrame().setAlwaysOnTop(true);
		// smoothCanvas.getFrame().removeNotify();
		// smoothCanvas.getFrame().setUndecorated(true);
		// smoothCanvas.getFrame().setLocation(0, 0);

		// adjustedPosition = true;
		// println("Adjusted");
		// }
		check = loadImage("Check.png");
		textBackground = loadImage("TextBackground.png");
		deleteButton = loadImage("DeleteButton.png");
		eraserImage = loadImage("EraserCursor.png");
		exportWordsearch = loadImage("ExportWordsearch.png");
		newWordsearch = loadImage("NewWordsearch.png");
		showCharacters = loadImage("ShowCharacters.png");
		tintToggle = loadImage("TintToggle.png");
		copyWords = loadImage("Copy.png");
		pasteWords = loadImage("Paste.png");
		errorIcon = loadImage("ErrorIcon.png");
		tintColour = color(226, 237, 255);
		int startX = (int) ((120 - newWordsearch.width) / 2f) + 5, startY = startX, spacing = 16;
		buttons.add(new NewWordsearchButton(startX, startY + (68 + spacing) * 0));
		buttons.add(new ExportButton(startX, startY + (68 + spacing) * 1));
		buttons.add(new ShowCharButton(startX, startY + (68 + spacing) * 2));
		buttons.add(new TintButton(startX, startY + (68 + spacing) * 3));
		buttons.add(new CopyButton(startX, startY + (68 + spacing) * 4));
		buttons.add(new PasteButton(startX, startY + (68 + spacing) * 5));

		fileLocation = System.getProperty("user.home") + "/Wordsearch/";
		// new
		// File("%appdata%\\.minecraft").getAbsolutePath();
		// println(fileLocation);

		// exec("C:/Users/Ryan/AppData/Roaming/.minecraft");

		arial = createFont("ARIAL.TFF", 64);
		lSans = createFont("LSANS.TTF", 64);

		// activeDialog = createWordsearchCreation();
		createWordsearch("", 15, 15, false);

		// String characters = "abcdefghijklmonpqrstuvwxyz";
		//
		// for(int i=0; i<15; i++)
		// {
		// wordBoard.wordBank.addWord(new String(new char[15]).replace("\0",
		// characters.substring(i, i+1)));
		// }
	}

	public List<String> orderWordsByLength(List<String> words)
	{
		List<String> newWords = new ArrayList<>();

		return newWords;
	}

	public Dialog createWordsearchCreation()
	{
		int boxWidth = 225, boxHeight = 130;
		return new Dialog("Create your wordsearch", boxWidth, boxHeight, "", new DialogObject[] {
				new TextInput("widthBox", TextInputType.NUMERICAL, 8, 38, (int) (boxWidth / 2f - 16), 22, "Columns"),
				new TextInput("heightBox", TextInputType.NUMERICAL, (int) (boxWidth / 2f + 8) + 1, 38, (int) (-boxWidth / 2f - 16), 22, "Rows"),
				new CheckButton("blueTintCheckButton", 8 + 6 + 6, 78, "Blue tint?", wordBoard != null && wordBoard.tint), new TextButton("cancel", 8, -32, 96, 24, "Cancel"),
				new TextButton("create", -8 - 96, -32, 96, 24, "Create") });
	}

	public Dialog createAreYouSure(String name, String description)
	{
		int boxWidth = 225, boxHeight = 150;
		return new Dialog("", boxWidth, boxHeight, "", new DialogObject[] {
				new TextInput("widthBox", TextInputType.NUMERICAL, 8, 38, (int) (boxWidth / 2f - 16), 22, "Columns"),
				new TextInput("heightBox", TextInputType.NUMERICAL, (int) (boxWidth / 2f + 8) + 1, 38, (int) (-boxWidth / 2f - 16), 22, "Rows"),
				new TextButton("cancel", 8, -32, 96, 24, "Cancel"), new TextButton("create", -8 - 96, -32, 96, 24, "Create") });
		
		
	}

	public void draw()
	{
		background(255);

		for (IconButton button : buttons)
		{
			button.draw();
		}

		stroke(0);
		strokeWeight(5);
		rectMode(CORNER);
		noFill();
		super.rect(2, 2, width - 5, height - 5, 1);
		// println(.textFont.toString());
		if (wordBoard != null)
			wordBoard.draw();

		if (activeDialog != null)
			activeDialog.draw();

		textSize(14);
		textAlign(CENTER, CENTER);
		int frameModulus = ((interval + tipFrameCount) % (framesPerTip + interval)) - interval;

		// if (frameModulus > framesPerTip || frameModulus <= 0)
		// {
		// fill(0, 0);
		// }
		// else
		// {
		// if (frameModulus < fadeInOutTime)
		// {
		// fill(0, (int) (frameModulus * 255 / (float) fadeInOutTime));
		// }
		// else
		// {
		// if (frameModulus > framesPerTip - fadeInOutTime)
		// {
		// fill(0, 255 - (int) ((((float) (frameModulus - (framesPerTip -
		// fadeInOutTime)) / (float) fadeInOutTime) * 255)));
		// }
		// else
		// {
		// fill(0, 255);
		// }
		// }
		// }

		fill(0,
				frameModulus > framesPerTip || frameModulus <= 0 ? 0
						: (frameModulus < fadeInOutTime ? (int) (frameModulus * 255 / (float) fadeInOutTime)
								: (frameModulus > framesPerTip - fadeInOutTime ? 255 - (int) ((((float) (frameModulus - (framesPerTip - fadeInOutTime)) / (float) fadeInOutTime) * 255))
										: 255)));
		// fill(0, frameModulus > framesPerTip || frameModulus <= 0 ? 255 :
		// (frameModulus < fadeInOutTime ? 255 - (int) (frameModulus * 255 /
		// (float) fadeInOutTime)
		// : (frameModulus > framesPerTip - fadeInOutTime ? (int)
		// (((framesPerTip - (framesPerTip - fadeInOutTime)) / fadeInOutTime) *
		// 255) : 0)));
		text("TIP: " + tips[tipCounter], width / 2, height - 22);

		int newTipCounter = ((int) ((float) tipFrameCount + interval) / (framesPerTip + interval) + offsetCounter) % tips.length;
		if (newTipCounter != tipCounter)
			tipCounter = newTipCounter;

		tipFrameCount++;
	}

	public void rect(int x, int y, int boxWidth, int boxHeight, int thickness)
	{
		strokeWeight(1);
		for (int i = 0; i < thickness; i++)
		{
			int offset = g.rectMode == CENTER ? 0 : i;
			rect(x + offset, y + offset, boxWidth - i * 2, boxHeight - i * 2);
		}
	}

	public void rrect(float x, float y, float boxWidth, float boxHeight, float rounded)
	{
		super.rect(x, y, boxWidth, boxHeight, rounded);
	}

	public void deleteIndex(int index)
	{
		words.remove(index);
		if (wordBoard.wordIndex == index)
			wordBoard.wordIndex = -1;
		else if (wordBoard.wordIndex > index)
			wordBoard.wordIndex -= 1;
		wordBoard.wordBank.delete(index);
		// wordBoard.wordBank.updateWords();
	}

	public void keyPressed()
	{
		keysPressed.add(keyCode);
		// println(keysPressed);
		if (activeDialog != null)
		{
			activeDialog.keyPressed();
		}
		if (wordBoard != null)
		{
			if (!wordBoard.nameInputBox.keyPressed())
				wordBoard.wordBank.keyPressed();
		}

		if (keysPressed.contains(CONTROL))
		{
			if (keyCode == 78)
				activeDialog = createWordsearchCreation();
			else if (keyCode == 77)
			{
				outputToPdf();
				keysPressed.clear();
			}
			else if (keyCode == 86)
			{
				wordBoard.wordBank.pasteWords();
			}
			else if (keyCode == 67)
			{
				wordBoard.wordBank.copyWords();
			}
		}

		if (keyCode == ESC)
		{
			keyCode = -1;
			key = '\0';
		}
		// if (keysPressed.contains(CONTROL) &&
	}

	public String removeIllegalCharacters(String string, TextInputType type)
	{
		if (string == null || type == null)
			return null;
		StringBuilder builder = new StringBuilder();
		for (char c : string.toCharArray())
		{
			if (c == '�')
				c = '\'';
			if (type.isValid(c))
				builder.append(c);
		}
		return builder.toString();
	}

	public void keyReleased()
	{
		keysPressed.remove(keyCode);
	}

	public void mouseWheel(MouseEvent event)
	{
		float e = event.getCount();
		if (wordBoard != null)
			wordBoard.mouseWheel(e);
	}

	public void mousePressed()
	{
		mouseXStart = mouseX;
		mouseYStart = mouseY;
		boolean doublePress = false;
		if (lastMousePress > System.currentTimeMillis() - 500)
		{
			doublePress = true;
		}
		lastMousePress = System.currentTimeMillis();
		if (activeDialog == null)
			for (IconButton button : buttons)
			{
				button.mousePressed();
			}
		if (activeDialog != null)
		{
			activeDialog.mousePressed();
		}
		else if (mouseY > height - 34)
		{
			if (mouseButton == LEFT)
			{
				offsetCounter++;
				tipFrameCount = fadeInOutTime;
			}
		}
		else if (wordBoard != null)
		{
			wordBoard.mousePressed();
		}
		updateCursor();
	}

	public void mouseReleased()
	{
		if (activeDialog != null)
		{
			activeDialog.mouseReleased();
		}
		else
		{
			wordBoard.mouseReleased();
		}
		updateCursor();
	}

	public void createWordsearch(String title, int columns, int rows, boolean tint)
	{
		wordBoard = new WordBoard(title, columns, rows, tint);
	}

	public List<Coordinate> getSelectedWordCells(Coordinate origin)
	{
		List<Coordinate> coords = new ArrayList<>();
		for (int i = 0; i < wordBoard.getSelectedWord(false).length(); i++)
		{
			coords.add(origin.getRelative(i, 0));
		}
		return coords;
	}

	public class Letter
	{
		String letter;
		int orientation, x, y;

		public Letter(String letter, int x, int y, int orientation)
		{
			this.letter = letter;
			this.x = x;
			this.y = y;
			this.orientation = orientation;
		}

		public Letter(char letter, int x, int y, int orientation)
		{
			this("" + letter, x, y, orientation);
		}

		public String toString()
		{
			return letter;
		}
	}

	public class Word
	{
		// String word;
		List<Letter> letters = new ArrayList<>();
		int x, y, x2, y2, orientation;
		boolean placed = false;
		String word, correctWord;

		public Word(String word, int x, int y, int orientation)
		{
			this.x = x;
			this.y = y;
			this.orientation = orientation;
			this.word = word;
			this.correctWord = wordBoard.removePunctuation(word);

			Coordinate relative = wordBoard.createRelative(orientation);
			this.x -= wordBoard.characterOffset * relative.x;
			this.y -= wordBoard.characterOffset * relative.y;
			x2 = this.x + relative.x * (correctWord.length() - 1);
			y2 = this.y + relative.y * (correctWord.length() - 1);
			char[] charArray = correctWord.toCharArray();
			for (int i = 0; i < charArray.length; i++)
			{
				char c = charArray[i];
				if (c != '\'')
					letters.add(new Letter(c, this.x + (relative.x * i), this.y + (relative.y * i), orientation));
			}
		}

		public int length()
		{
			return letters.size();
		}

		public Letter charAt(int i)
		{
			return letters.get(i);
		}

		public String toString()
		{
			StringBuilder builder = new StringBuilder();
			for (Letter letter : letters)
			{
				builder.append(letter.letter);
			}
			return builder.toString();
		}
	}

	public void outputToPdf()
	{
		String fileName = wordBoard.getFileName();
		println(fileName);
		String name = wordBoard.nameInputBox.text;
		int pageHeight = 2100, pageWidth = (int) (pageHeight / 1.414f);
		PGraphics pdf = createGraphics(pageWidth, pageHeight, PDF, fileName);
		Map<Coordinate, Letter> letters = wordBoard.getAllLetterCoords();
		float margin = 0.15f, wordsearchWidth = (pageWidth * (1.f - (margin * 2))), wordsearchHeight;
		pdf.beginDraw();
		pdf.pushMatrix();
		float topX = pageWidth * margin, topY = pageHeight * 0.02285f;
		pdf.translate(topX, topY);
		int includeName = name != null && name.length() > 0 ? 1 : 0;
		float originalSize = pageHeight * 0.0353f;
		float nameTextSize = includeName * originalSize;
		float nameHeight = includeName * originalSize * 1.09f;
		pdf.textAlign(CENTER, CENTER);
		if (includeName == 1)
		{
			pdf.textSize(nameTextSize);
			float textWidth = pdf.textWidth(name);
			if (textWidth > wordsearchWidth)
				nameTextSize = wordsearchWidth * nameTextSize / textWidth;
			pdf.fill(0);
			pdf.textSize(nameTextSize);
			pdf.text(name, wordsearchWidth / 2, nameHeight * 0.9f / 2);
			pdf.translate(0, nameHeight * 1.45f);
		}
		float cellSize = Math.min(wordsearchWidth / wordBoard.rows, wordsearchWidth / wordBoard.columns), cellWidth = cellSize, cellHeight = cellSize, lineWidth = Math.min(
				cellSize / 32f, pageHeight / 350f);
		wordsearchHeight = cellHeight * wordBoard.rows;
		pdf.strokeWeight(lineWidth);
		pdf.stroke(0);
		for (int x = 0; x < wordBoard.columns; x++)
			for (int y = 0; y < wordBoard.rows; y++)
			{
				Coordinate coord = new Coordinate(x, y);
				float xPos = x * cellWidth, yPos = y * cellHeight;
				if (wordBoard.tint)
					pdf.fill(tintColour);
				else
					pdf.noFill();
				pdf.rect(xPos, yPos, cellWidth, cellHeight);
				pdf.fill(0);
				String letter = null;
				if (letters.containsKey(coord))
					letter = letters.get(coord).toString();
				else
					letter = wordBoard.generatedCharacters[y][x];
				pdf.textSize(cellHeight * 3 / 5f);
				pdf.text(letter, xPos + cellWidth / 2f, yPos + cellHeight / 2.4f);
			}
		pdf.noFill();
		pdf.strokeWeight(lineWidth * 2);
		pdf.rect(-lineWidth / 2f, -lineWidth / 2f, wordsearchWidth + lineWidth, wordsearchHeight + lineWidth);

		float wordSpace = pageHeight - wordsearchHeight - (nameHeight * 2) - topY * 3;
		float lineSpacing = (wordSpace + (nameHeight * 2)) / (Math.max(15, wordBoard.wordObjects.size()));
		pdf.textAlign(LEFT, CORNER);
		pdf.textSize(lineSpacing * 2 / 3);
		pdf.fill(0);
		pdf.strokeWeight(pageHeight / 1050f);
		for (int i = 0; i < wordBoard.wordObjects.size(); i++)
		{
			float textY = wordsearchHeight + topY * 2 + (lineSpacing * i);
			String text = wordBoard.wordObjects.get(i).word.replace("-", "- ");
			pdf.text(text, 0, textY);
			pdf.line(pdf.textWidth(text) + topY / 3, textY, pageWidth - (topX * 2), textY);
		}
		pdf.popMatrix();
		pdf.fill(204);
		float textSize = (12f / 950f) * pageHeight;
		pdf.textSize(textSize);
		pdf.textAlign(RIGHT, BOTTOM);
		pdf.text(fileName, pdf.width - (textSize / 2), pdf.height - (textSize / 2));
		pdf.dispose();
		pdf.endDraw();
		process = launch(fileName);
	}

	public abstract class IconButton
	{
		protected int xPos, yPos, captionX, captionY, textSize = 12;
		protected PImage image, greyImage;
		protected boolean active = true, mouseOver = false;
		protected String caption = "Button";

		public IconButton(PImage image, String caption, int xPos, int yPos)
		{
			this.image = image;
			this.caption = caption;
			greyImage = new PImage(image.getImage());
			greyImage.filter(GRAY);
			this.xPos = xPos;
			this.yPos = yPos;
			captionX = (int) (image.width / 2f);
			captionY = (int) (image.height / 2f);
			while (captionY < image.height && alpha(image.get(captionX, captionY)) != 0)
				captionY += 1;

			captionX += xPos;
			captionY += yPos + textSize + 2;

		}

		public abstract void mousePressed();

		public void draw()
		{
			mouseOver = mouseOver();
			imageMode(CORNER);
			tint(mouseOver && active ? 235 : active ? 255 : 220);
			image(active ? image : greyImage, xPos, yPos);
			if (active && mouseOver)
			{
				fill(100);
				textAlign(CENTER, BOTTOM);
				textFont(lSans);
				textSize(textSize);
				text(caption, captionX, captionY);
			}
		}

		public boolean mouseOver()
		{
			return activeDialog == null && mouseX >= xPos && mouseX < xPos + image.width && mouseY >= yPos && mouseY < yPos + image.height
					&& alpha(image.get(mouseX - xPos, mouseY - yPos)) != 0;
		}
	}

	public class NewWordsearchButton extends IconButton
	{
		public NewWordsearchButton(int xPos, int yPos)
		{
			super(newWordsearch, "New Wordsearch", xPos, yPos);
		}

		@Override
		public void mousePressed()
		{
			lastMousePress = -1;
			if (active && mouseOver())
			{
				if (keysPressed.contains(SHIFT))
					createWordsearch(wordBoard != null && wordBoard.nameInputBox != null ? wordBoard.nameInputBox.text : "", wordBoard != null ? wordBoard.columns : 15,
							wordBoard != null ? wordBoard.rows : 15, wordBoard != null ? wordBoard.tint : false);
				else
					activeDialog = createWordsearchCreation();
			}
		}

		@Override
		public void draw()
		{
			super.draw();
		}
	}

	public class ExportButton extends IconButton
	{
		public ExportButton(int xPos, int yPos)
		{
			super(exportWordsearch, "Export to PDF", xPos, yPos);
		}

		@Override
		public void mousePressed()
		{
			if (active && mouseOver())
				outputToPdf();
		}

		@Override
		public void draw()
		{
			active = wordBoard != null;
			super.draw();
		}
	}

	public class ShowCharButton extends IconButton
	{
		public ShowCharButton(int xPos, int yPos)
		{
			super(showCharacters, "Show characters", xPos, yPos);
		}

		@Override
		public void mousePressed()
		{
			if (wordBoard != null && mouseOver())
				wordBoard.generateCharacters();
		}

		@Override
		public void draw()
		{
			super.draw();
			active = wordBoard != null;
			if (active)
				wordBoard.showCharacters = mouseOver();
		}
	}

	public class TintButton extends IconButton
	{
		public TintButton(int xPos, int yPos)
		{
			super(tintToggle, "Toggle tint", xPos, yPos);
		}

		@Override
		public void mousePressed()
		{
			if (wordBoard != null && mouseOver())
			{
				wordBoard.tint = !wordBoard.tint;
			}
		}

		@Override
		public void draw()
		{
			super.draw();
			active = wordBoard != null;
		}
	}

	public class CopyButton extends IconButton
	{
		public CopyButton(int xPos, int yPos)
		{
			super(copyWords, "Copy words", xPos, yPos);
		}

		@Override
		public void mousePressed()
		{
			if (wordBoard != null && mouseOver())
			{
				wordBoard.wordBank.copyWords();
			}
		}

		@Override
		public void draw()
		{
			super.draw();
			active = wordBoard != null;
		}
	}

	public class PasteButton extends IconButton
	{
		public PasteButton(int xPos, int yPos)
		{
			super(pasteWords, "Paste words", xPos, yPos);
		}

		@Override
		public void mousePressed()
		{
			if (wordBoard != null && mouseOver())
			{
				wordBoard.wordBank.pasteWords();
			}
		}

		@Override
		public void draw()
		{
			super.draw();
			active = wordBoard != null;
		}
	}

	public class WordBoard
	{
		LetterCell[][] cells;
		public int topX, topY, rightX = 0, columns, rows, boxWidth, boxHeight;
		float cellWidth, cellHeight, lineWidth;

		private int wordIndex = -1, orientation = 2, characterOffset = 2;

		private List<Word> wordObjects = new ArrayList<>(), wordsUnderMouse = new ArrayList<>();
		private int mouseLayer = 0;

		private WordBank wordBank;
		private NameInputBox nameInputBox;
		private boolean tint;
		private String title = "";
		private boolean dragging = false;

		// private CellEligibility[][] eligiblePositions;
		private Map<Coordinate, CellEligibility> eligiblePositions = new HashMap<>();

		private Coordinate mousePos = null, previousMousePos = null;
		public String[][] generatedCharacters;

		public boolean showCharacters = false;

		public WordBoard(String title, int columns, int rows, boolean tint)
		{
			// middleX = width / 2;
			// middleY = height / 2 - 96;

			this.title = title;
			this.columns = columns;
			this.rows = rows;
			this.tint = tint;
			int heightOfNameBox = 64;

			topY = 24 + heightOfNameBox;
			topX = 120;
			boxWidth = height - topY - heightOfNameBox / 2;
			boxHeight = boxWidth;
			rightX = topX + boxWidth + 24;

			int savedBoxWidth = boxWidth;
			int sizeOfCell = (int) (Math.min((boxWidth / (float) columns), (int) (boxHeight / (float) rows)));
			cellWidth = sizeOfCell;
			cellHeight = sizeOfCell;
			boxWidth = (int) cellWidth * columns;
			boxHeight = (int) cellHeight * rows;
			lineWidth = (int) cellWidth / 32f;
			wordBank = new WordBank(rightX, 24, savedBoxWidth);

			topY += (int) ((savedBoxWidth - boxHeight) / 2f);
			topX += (int) ((savedBoxWidth - boxWidth) / 2f);
			nameInputBox = new NameInputBox(title, (int) (topX + boxWidth / 2.0), topY / 2, boxWidth, heightOfNameBox);

			// (int) (width / 2f - boxWidth / 2f);
			cells = new LetterCell[rows][columns];
			generatedCharacters = new String[rows][columns];
			for (int x = 0; x < columns; x++)
			{
				for (int y = 0; y < rows; y++)
				{
					cells[y][x] = new LetterCell(this, x, y, (int) cellWidth, (int) cellHeight);
					setGeneratedCharacter(x, y);
				}
			}

			updateEligiblePositions();
		}

		public String getFileName()
		{
			String path = fileLocation + "pdf/";
			String name;
			if (nameInputBox.text == null || nameInputBox.text.equals(""))
			{
				name = "output " + String.format("%02d", day()) + "." + String.format("%02d", month()) + "." + String.format("%02d", (year() % 100)) + " "
						+ String.format("%02d", hour()) + "-" + String.format("%02d", minute()) + "-" + String.format("%02d", second());
			}
			else
			{
				name = nameInputBox.text;
			}
			int number = 1;
			File destinationFile;
			while ((destinationFile = new File(path + name + (number > 1 ? " (" + number + ")" : "") + ".pdf")).exists())
			{
				number += 1;
			}
			return destinationFile.getAbsolutePath().replace("\\", "/");
		}

		public void setGeneratedCharacter(int x, int y)
		{
			generatedCharacters[y][x] = new String(generateCharacter());
		}

		public void generateCharacters()
		{
			for (int x = 0; x < columns; x++)
			{
				for (int y = 0; y < rows; y++)
				{
					setGeneratedCharacter(x, y);
				}
			}
		}

		public String returnWords()
		{
			StringBuilder builder = new StringBuilder();

			for (int i = 0; i < words.size(); i++)
			{
				Word word = getWordObject(wordBoard.getWord(i, true));
				builder.append(words.get(i) + (word != null ? "%i%" + word.x + "|" + word.y + "|" + word.orientation : "") + (i < words.size() - 1 ? "\n" : ""));
			}

			return builder.toString();
		}

		public void draw()
		{
			strokeWeight(1);
			previousMousePos = mousePos;
			mousePos = reversePosition(mouseX, mouseY);

			if ((previousMousePos != null && mousePos == null) || (mousePos != null && (previousMousePos == null || !previousMousePos.equals(mousePos))))
				mouseMoved();

			nameInputBox.draw();

			boolean displayWord = mousePos != null && activeDialog == null;
			Coordinate relative = createRelative(orientation);
			Map<LetterCell, String> mouseLetterCoords = new HashMap<>();
			// int dragOffset = 2;
			if (displayWord && wordIndex != -1)
				for (int i = 0; i < getSelectedWord(false).length(); i++)
				{
					Coordinate relativeCoord = mousePos.getRelative(relative.x * (i - characterOffset), relative.y * (i - characterOffset));
					if (relativeCoord.x >= 0 && relativeCoord.y >= 0 && relativeCoord.x < cells[0].length && relativeCoord.y < cells.length)
						mouseLetterCoords.put(cells[relativeCoord.y][relativeCoord.x], "" + getSelectedWord(false).charAt(i));
				}

			Map<Coordinate, Letter> letterCoords = new HashMap<>();
			for (Word word : wordObjects)
			{
				letterCoords.putAll(getCellPositions(word));
				for (Coordinate coord : letterCoords.keySet())
				{
					cells[coord.y][coord.x].setNextCharacter(letterCoords.get(coord).toString());
					cells[coord.y][coord.x].setNextColor(color(100));
				}
			}

			for (int x = 0; x < columns && displayWord; x++)
				for (int y = 0; y < rows && displayWord; y++)
				{
					Coordinate coord = new Coordinate(x, y);
					// String[] character = new String[0];
					if (mousePos != null)
					{
						if (mousePos.x == x && mousePos.y == y)
						{
							if (eligiblePositions.containsKey(coord) && eligiblePositions.get(coord) == CellEligibility.INELIGIBLE)
								displayWord = false;
						}
					}
				}

			for (int x = 0; x < columns; x++)
				for (int y = 0; y < rows; y++)
				{
					Coordinate coord = new Coordinate(x, y);
					// String[] character = new String[0];
					fill(255);

					if (mouseLetterCoords.containsKey(cells[y][x]))
					{
						cells[y][x].setNextColor(!displayWord ? color(204, 0, 0) : 0);
						cells[y][x].setNextCharacter(mouseLetterCoords.get(cells[y][x]));
					}
					else if (showCharacters && !letterCoords.containsKey(new Coordinate(x, y)))
					{
						cells[y][x].setNextColor(color(204, 0, 0, 100));
						cells[y][x].setNextCharacter(generatedCharacters[y][x]);
					}
					cells[y][x].draw();
				}
			// strokeWeight(2);
			rectMode(CORNER);
			noFill();
			stroke(0);
			rect(topX, topY, boxWidth, boxHeight, 2);

			if (wordIndex != -1 && mousePos != null)
			{
				fill(0, 20);
				// strokeWeight(2);
				noStroke();
				drawRectangle(mousePos.x, mousePos.y, mousePos.x, mousePos.y);
			}

			if (wordBank != null)
				wordBank.draw();

			stroke(204, 0, 0);
			noFill();
			// for(Word word : wordsUnderMouse)
			// {
			if (wordIndex == -1 && wordsUnderMouse.size() > 0 && mouseLayer() < wordsUnderMouse.size())
			{
				Word word = wordsUnderMouse.get(mouseLayer());
				drawWordRectangle(word.x, word.y, word.x2, word.y2);
			}
			// }

			// stroke(255, 0, 0);
			// strokeWeight(2);
			// drawRectangle(2, 2, 5, 5);
		}

		private int mouseLayer()
		{
			return Math.min(Math.max(0, mouseLayer), wordsUnderMouse.size() - 1);
		}

		public void mouseMoved()
		{
			wordsUnderMouse = new ArrayList<>();
			if (wordIndex == -1 && mousePos != null)
			{
				// mouseLayer() = 0;
				for (Word wordObject : wordObjects)
				{
					boolean found = false;
					for (int i = 0; i < wordObject.letters.size() && !found; i++)
						if (wordObject.letters.get(i).x == mousePos.x && wordObject.letters.get(i).y == mousePos.y)
						{
							found = true;
							wordsUnderMouse.add(wordObject);
						}
				}
			}
			if (mousePressed && mouseButton == RIGHT)
			{
				removeWordsUnderMouse();
			}

			updateCursor();
		}

		public void removeWordsUnderMouse()
		{
			for (Word wordObject : wordsUnderMouse)
			{
				wordBoard.wordObjects.remove(wordObject);
			}
			wordBoard.wordBank.updateWords();
			wordsUnderMouse.clear();
		}

		public Word getWordObject(String word)
		{
			for (Word wordObject : wordObjects)
				if (wordObject.word.equals(word))
					return wordObject;
			return null;
		}

		public int getWordIndex(Word wordObject)
		{
			for (int i = 0; i < words.size(); i++)
			{
				String word = words.get(i);
				if (wordObject.word.equals(word))
					return i;
			}
			return -1;
		}

		public boolean containsWord(String word)
		{
			return getWordObject(word) != null;
		}

		public String getSelectedWord(boolean showPunctuation)
		{
			return getWord(wordIndex, showPunctuation);
		}

		public String getWord(int i, boolean showPunctuation)
		{
			String theWord = words.get(i);
			if (!showPunctuation)
				theWord = removePunctuation(theWord);
			return theWord;
		}

		public String removePunctuation(String word)
		{
			return word.replace("\'", "").replace("-", "");
		}

		public Map<Coordinate, CellEligibility> getEligiblePositions()
		{
			return wordIndex == -1 ? new HashMap<Coordinate, CellEligibility>() : getEligiblePositions(getSelectedWord(false), orientation);
		}

		public Map<Coordinate, CellEligibility> getEligiblePositions(String word, int wordOrientation)
		{
			Map<Coordinate, CellEligibility> cellEligibilities = new HashMap<>();
			int lengthOfTheWord = word.length();
			Coordinate relative = createRelative(wordOrientation);
			Map<Coordinate, Letter> letterPositions = getAllLetterCoords();

			for (int x = 0; x < columns; x++)
			{
				for (int y = 0; y < rows; y++)
				{
					Coordinate cellCoord = new Coordinate(x, y);
					CellEligibility cellEligibility = CellEligibility.ELIGIBLE;
					// if (x + relative.x * lengthOfTheWord < -1 || y +
					// relative.y * lengthOfTheWord < -1 || x > columns -
					// relative.x * lengthOfTheWord
					// || y > rows - relative.y * lengthOfTheWord)

					// if (x x - relative.x * (characterOffset -
					// lengthOfTheWord) < (characterOffset - 1) * relative.x
					// || y - relative.y * (characterOffset - lengthOfTheWord) <
					// (characterOffset - 1) * relative.y
					// || x + relative.x * (lengthOfTheWord - characterOffset) >
					// columns || y + relative.y * (lengthOfTheWord -
					// characterOffset) > rows)

					// if (x < (characterOffset - 1) * relative.x || y -
					// relative.y * (characterOffset - lengthOfTheWord) <
					// (characterOffset - 1) * relative.y
					// || x + relative.x * (lengthOfTheWord - characterOffset) >
					// columns || y + relative.y * (lengthOfTheWord -
					// characterOffset) > rows)

					Coordinate firstCharacter = new Coordinate(x - characterOffset * relative.x, y - characterOffset * relative.y);
					Coordinate lastCharacter = new Coordinate(x + ((lengthOfTheWord - 1) - characterOffset) * relative.x, y + ((lengthOfTheWord - 1) - characterOffset)
							* relative.y);

					if (!(firstCharacter.x >= 0 && firstCharacter.x < columns && firstCharacter.y >= 0 && firstCharacter.y < rows && lastCharacter.x >= 0
							&& lastCharacter.x < columns && lastCharacter.y >= 0 && lastCharacter.y < rows))
						cellEligibility = CellEligibility.INELIGIBLE;
					else
					{
						int overlap = 0;
						for (int i = 0; i < lengthOfTheWord && cellEligibility != CellEligibility.INELIGIBLE; i++)
						{
							Coordinate coord = new Coordinate(x + (relative.x * (i - characterOffset)), y + (relative.y * (i - characterOffset)));
							if (letterPositions.containsKey(coord))
							{
								Letter character = letterPositions.get(coord);
								if (character.toString().equals("" + word.charAt(i)) && character.orientation != wordOrientation)
								{
									cellEligibility = CellEligibility.PREFERED;
									overlap += 1;
								}
								else
									cellEligibility = CellEligibility.INELIGIBLE;
							}
						}
						if (overlap == lengthOfTheWord)
							cellEligibility = CellEligibility.INELIGIBLE;
					}
					cellEligibilities.put(cellCoord, cellEligibility);
				}
			}
			return cellEligibilities;
		}

		public Map<Coordinate, Letter> getAllLetterCoords()
		{
			Map<Coordinate, Letter> letterCoords = new HashMap<>();
			for (Word word : wordObjects)
				letterCoords.putAll(getCellPositions(word));
			return letterCoords;
		}

		public void mousePressed()
		{
			if (mouseButton == LEFT)
			{
				if (nameInputBox.mouseOver())
				{
					nameInputBox.mousePressed();
					wordBank.addNewWord.active = false;
				}
				else
				{
					nameInputBox.active = false;
					if (mouseOver())
					{
						if (wordIndex == -1)
						{
							if (wordsUnderMouse.size() > 0)
							{
								Coordinate mousePos = reversePosition(mouseX, mouseY);
								Word wordObject = wordsUnderMouse.get(mouseLayer());
								wordsUnderMouse.clear();
								// mouseLayer() = 0;
								int index = getWordIndex(wordObject);
								wordIndex = index;
								dragging = true;
								orientation = wordObject.orientation;
								wordObjects.remove(wordObject);
								characterOffset = new Coordinate(wordObject.x, wordObject.y).getOffset(mousePos);
								updateEligiblePositions();
								wordBoard.wordBank.updateWords();
							}
						}
						else
						{
							attemptToPlaceWord();
						}
					}
					else
						wordBank.mousePressed();
				}
			}
			else if (mouseButton == RIGHT)
			{
				if (wordBoard.mouseOver())
					removeWordsUnderMouse();
			}
		}

		public void mouseReleased()
		{
			if (dragging || mouseOver())
			{
				attemptToPlaceWord();
				dragging = false;
			}
			mouseMoved();
		}

		public void attemptToPlaceWord()
		{
			Coordinate mouse = reversePosition(mouseX, mouseY);
			// println(mouse.x + ", " + mouse.y);
			if (wordIndex != -1 && mouse != null && eligiblePositions.containsKey(mouse) && eligiblePositions.get(mouse) != CellEligibility.INELIGIBLE)
			{
				wordObjects.add(new Word(getSelectedWord(true), mouse.x, mouse.y, orientation));
				wordBank.updateWords();
				wordIndex = -1;
				updateEligiblePositions();
			}
			else if (mouse == null)
			{
				wordIndex = -1;
				updateEligiblePositions();
			}
		}

		public void attemptToPlaceWord(String string, int x, int y, int orientation)
		{
			Coordinate position = new Coordinate(x, y);
			// int oldOrientation = this.orientation;
			// this.orientation = orientation;
			updateEligiblePositions(string, orientation);
			wordBoard.characterOffset = 0;
			if (eligiblePositions.containsKey(position) && eligiblePositions.get(position) != CellEligibility.INELIGIBLE)
			{
				wordObjects.add(new Word(string, position.x, position.y, orientation));
				println("placed");
				wordBank.updateWords();
			}
			else
			{
				println("couldn't be placed");
			}
			// this.orientation = oldOrientation;
			updateEligiblePositions();
		}

		public boolean mouseOver()
		{
			return mouseX >= topX && mouseX < topX + boxWidth && mouseY >= topY && mouseY < topY + boxHeight;
		}

		public void mouseWheel(float e)
		{
			if (wordIndex != -1)
			{
				if (e < 0)
					orientation = Math.max(-1, orientation - 1);
				else if (e > 0)
					orientation = Math.min(2, orientation + 1);

				if (e != 0)
					updateEligiblePositions();
			}
			else if (wordsUnderMouse.size() != 0)
			{
				mouseLayer = (mouseLayer + (e < 0 ? +1 : (e > 0 ? -1 : 0))) % wordsUnderMouse.size();
				while (mouseLayer < 0)
					mouseLayer += wordsUnderMouse.size();
				// println(mouseLayer);
			}
		}

		private Coordinate createRelative(int direction)
		{
			if (direction == -1)
				return new Coordinate(1, -1);
			else if (direction == 0)
				return new Coordinate(1, 0);
			else if (direction == 1)
				return new Coordinate(1, 1);
			else if (direction == 2)
				return new Coordinate(0, 1);
			return null;
		}

		public void updateEligiblePositions()
		{
			eligiblePositions = getEligiblePositions();
			boolean somethingSelected = wordIndex != -1;
			for (int x = 0; x < columns; x++)
				for (int y = 0; y < rows; y++)
				{
					Coordinate coord = new Coordinate(x, y);
					// if(eligiblePositions[y][x] != CellEligibility.INELIGIBLE)

					// else
					// cells[y][x].overlay.setActive(false);
					if (somethingSelected)
					{
						cells[y][x].overlay.eligibility = eligiblePositions.get(coord);
					}
					cells[y][x].overlay.setActive(somethingSelected);
				}
		}

		public void updateEligiblePositions(String word, int orientation)
		{
			eligiblePositions = getEligiblePositions(removePunctuation(word), orientation);
			for (int x = 0; x < columns; x++)
				for (int y = 0; y < rows; y++)
				{
					Coordinate coord = new Coordinate(x, y);
					// if(eligiblePositions[y][x] != CellEligibility.INELIGIBLE)

					// else
					// cells[y][x].overlay.setActive(false);
					cells[y][x].overlay.eligibility = eligiblePositions.get(coord);
					cells[y][x].overlay.setActive(false);
				}
		}

		public Map<Coordinate, Letter> getCellPositions(Word word)
		{
			Map<Coordinate, Letter> coords = new HashMap<>();
			List<Coordinate> coordList = getLetterCoordinates(word);
			for (int i = 0; i < word.length(); i++)
				coords.put(coordList.get(i), word.charAt(i));
			return coords;
		}

		public List<Coordinate> getLetterCoordinates(Word word)
		{
			List<Coordinate> coords = new ArrayList<>();
			Coordinate relative = createRelative(word.orientation);
			for (int i = 0; i < word.length(); i++)
				coords.add(new Coordinate(word.x + relative.x * i, word.y + relative.y * i));
			return coords;
		}

		public Coordinate calculatePosition(int x, int y)
		{
			return new Coordinate(topX + (int) (x * cellWidth), topY + (int) (y * cellHeight));
		}

		public Coordinate reversePosition(int xPos, int yPos)
		{
			// println(cellWidth);
			if (xPos > topX && xPos < topX + boxWidth && yPos > topY && yPos < topY + boxHeight)
				return new Coordinate((int) ((xPos - topX) / cellWidth), (int) ((yPos - topY) / cellHeight));
			else
				return null;
		}

		public void drawWordRectangle(int x1, int y1, int x2, int y2)
		{
			stroke(255, 0, 0);
			strokeWeight(2);
			fill(255, 0, 0, 25);
			drawRectangle(x1, y1, x2, y2);
		}

		public void drawRectangle(int x1, int y1, int x2, int y2)
		{
			Coordinate topCoord = calculatePosition(x1, y1);
			Coordinate bottomCoord = calculatePosition(x2, y2);
			int direction;
			boolean straight = topCoord.x == bottomCoord.x || topCoord.y == bottomCoord.y;
			if (topCoord.x >= bottomCoord.x && topCoord.y < bottomCoord.y)
				direction = 0;
			else if (topCoord.x > bottomCoord.x && topCoord.y <= bottomCoord.y)
				direction = 1;
			else if (topCoord.x <= bottomCoord.x && topCoord.y > bottomCoord.y)
				direction = 2;
			else
				direction = 3;

			// off
			// ((direction == 0 ? a : (direction == 1 ? a : (direction == 2 ? a : a))) * cell)
			// on

			float dL = 9 / 32f;
			beginShape();
			vertex(topCoord.x + ((direction == 0 ? (straight ? 0 : dL) : (direction == 1 ? 1 : (direction == 2 ? 1 - (straight ? 0 : dL) : 0))) * cellWidth), topCoord.y
					+ ((direction == 0 ? 0 : (direction == 1 ? (straight ? 0 : dL) : (direction == 2 ? 1 : 1 - (straight ? 0 : dL)))) * cellHeight));
			vertex(topCoord.x + ((direction == 0 ? 1 : (direction == 1 ? 1 : (direction == 2 ? 0 : 0))) * cellWidth), topCoord.y
					+ ((direction == 0 ? 0 : (direction == 1 ? 1 : (direction == 2 ? 1 : 0))) * cellHeight));
			if (!straight)
				vertex(topCoord.x + ((direction == 0 ? 1 : (direction == 1 ? (straight ? 0 : dL) : (direction == 2 ? 0 : 1 - (straight ? 0 : dL)))) * cellWidth), topCoord.y
						+ ((direction == 0 ? 1 - (straight ? 0 : dL) : (direction == 1 ? 1 : (direction == 2 ? (straight ? 0 : dL) : 0))) * cellHeight));
			vertex(bottomCoord.x + ((direction == 0 ? 1 - (straight ? 0 : dL) : (direction == 1 ? 0 : (direction == 2 ? (straight ? 0 : dL) : 1))) * cellWidth), bottomCoord.y
					+ ((direction == 0 ? 1 : (direction == 1 ? 1 - (straight ? 0 : dL) : (direction == 2 ? 0 : (straight ? 0 : dL)))) * cellHeight));
			vertex(bottomCoord.x + ((direction == 0 ? 0 : (direction == 1 ? 0 : (direction == 2 ? 1 : 1))) * cellWidth), bottomCoord.y
					+ ((direction == 0 ? 1 : (direction == 1 ? 0 : (direction == 2 ? 0 : 1))) * cellHeight));
			if (!straight)
				vertex(bottomCoord.x + ((direction == 0 ? 0 : (direction == 1 ? 1 - (straight ? 0 : dL) : (direction == 2 ? 1 : (straight ? 0 : dL)))) * cellWidth),
						bottomCoord.y + ((direction == 0 ? (straight ? 0 : dL) : (direction == 1 ? 0 : (direction == 2 ? 1 - (straight ? 0 : dL) : 1))) * cellHeight));
			endShape(CLOSE);
		}

		public String generateCharacter()
		{
			String characters = "abcdefghijklmnopqrstuvwxyz";
			return characters.charAt(new Random().nextInt(characters.length())) + "";
		}
	}

	public class Coordinate
	{
		public int x, y;

		public Coordinate(int x, int y)
		{
			this.x = x;
			this.y = y;
		}

		public Coordinate getRelative(int x, int y)
		{
			return new Coordinate(this.x + x, this.y + y);
		}

		@Override
		public boolean equals(Object o)
		{
			boolean equal = false;
			if (o instanceof Coordinate)
				equal = ((Coordinate) o).x == x && ((Coordinate) o).y == y;
			// println("equals = " + equal + ". Hashcode: " + hashCode() +
			// "/" + o.hashCode());
			return equal;
		}

		@Override
		public int hashCode()
		{
			return x >> y;
		}

		public Coordinate difference(Coordinate coord)
		{
			return new Coordinate(x - coord.x, y - coord.y);
		}

		public int getOffset(Coordinate coord)
		{
			int offset = 0;
			Coordinate diff = difference(coord);
			offset = Math.max(Math.abs(diff.x), Math.abs(diff.y));
			return offset;
		}
	}

	private class CellOverlay
	{
		private boolean active = false;
		private int color;
		private CellEligibility eligibility;
		private int xPos, yPos, cellWidth, cellHeight, border = 0;

		public CellOverlay(int x, int y, int cellWidth, int cellHeight)
		{
			xPos = x + border;
			yPos = y + border;
			this.cellWidth = cellWidth - border * 2;
			this.cellHeight = cellHeight - border * 2;
		}

		public void draw()
		{
			if (active)
			{
				color = eligibility == CellEligibility.INELIGIBLE ? color(201, 0, 0) : (eligibility == CellEligibility.ELIGIBLE ? -1 : color(0, 0, 201));
				if (color == -1)
					noFill();
				else
					fill(color, 50);
				noStroke();
				rect(xPos + 1, yPos + 1, cellWidth - 1, cellHeight - 1);
			}
		}

		public void setColor(int color)
		{
			this.color = color;
			// this.active = true;
		}

		public int getColor()
		{
			return color;
			// this.active = true;
		}

		public void setActive(boolean isActive)
		{
			this.active = isActive;
		}
	}

	public class LetterCell
	{
		int x, y, xPos, yPos, cellWidth, cellHeight;
		// String character = null; // Math.random() < 0.3 ?
		// characters.charAt((int)(Math.random() * characters.length())) + "" :
		boolean shown;
		WordBoard board;
		CellOverlay overlay;
		String drawnCharacter;
		int color;

		public LetterCell(WordBoard board, int x, int y, int cellWidth, int cellHeight)
		{
			this.x = x;
			this.y = y;
			this.board = board;
			Coordinate location = board.calculatePosition(x, y);
			this.xPos = location.x;
			this.yPos = location.y;
			this.cellWidth = cellWidth;
			this.cellHeight = cellHeight;
			overlay = new CellOverlay(xPos, yPos, cellWidth, cellHeight);
			// overlay.setColor(color(201, 0, 0));
			overlay.setActive(true);
		}

		public void setNextCharacter(String drawnCharacter)
		{
			this.drawnCharacter = drawnCharacter;
		}

		public void setNextColor(int color)
		{
			this.color = color;
		}

		public void draw()
		{
			rectMode(CORNER);
			imageMode(CORNER);
			tint(255);
			fill(overlay.active || !wordBoard.tint ? 255 : tintColour);
			stroke(0);
			rect(xPos, yPos, cellWidth, cellHeight);

			if (drawnCharacter != null && drawnCharacter.length() > 0)
			{
				textFont(arial, cellHeight * 3 / 4f);
				textAlign(CENTER, CENTER);
				fill(color);
				text(drawnCharacter, xPos + cellWidth / 2f, yPos + cellHeight / 2.5f);
			}
			drawnCharacter = null;
			color = color(0);
			overlay.draw();
		}

		private boolean mouseOver()
		{
			return mouseX > xPos && mouseX < xPos + cellWidth && mouseY > yPos && mouseY < yPos + cellHeight;
		}

		public void mousePressed()
		{
			if (mouseOver() && !shown)
			{

			}
		}

		public void setShown(boolean isShown)
		{
			shown = isShown;
		}

		public boolean getShown()
		{
			return shown;
		}

		// public void setCharacter(String character)
		// {
		// this.character = character;
		// }

		public void pressed()
		{

		}
	}

	public abstract class DialogObject
	{
		int initialX, initialY, initialBoxWidth, initialBoxHeight, x, y, xPos, yPos, boxWidth, boxHeight;
		boolean active = false, selectable = true;
		String text = "", name = "n/a";
		Dialog dialog;

		public void setPositionAndSize()
		{
			if (initialBoxWidth < 0)
				this.boxWidth = dialog.boxWidth + initialBoxWidth;
			else
				this.boxWidth = initialBoxWidth;
			if (initialBoxHeight < 0)
				this.boxHeight = dialog.boxHeight + initialBoxHeight;
			else
				this.boxHeight = initialBoxHeight;
			if (initialX < 0)
				this.x = dialog.boxWidth + initialX;
			else
				this.x = initialX;
			if (initialY < 0)
				this.y = dialog.boxHeight + initialY;
			else
				this.y = initialY;
		}

		public void setActive(boolean isActive)
		{
			active = isActive;
		}

		public abstract void draw();

		public boolean isActive()
		{
			return active;
		}

		public boolean isInside(int x, int y)
		{
			return x > xPos && x < xPos + boxWidth && y > yPos && y < yPos + boxHeight;
		}

		public boolean mouseOver()
		{
			boolean mouseOver = isInside(mouseX, mouseY);
			// if(name.equals("advancedText"))
			// println(mouseOver);
			return mouseOver;
		}

		public abstract void mousePressed();

		public abstract void mouseReleased();

		public abstract void keyPressed();
	}

	public class Dialog
	{
		private String header = "Dialog";
		private String text = "Please input below.";
		private List<DialogObject> dialogObjects;
		private int x, y, boxWidth, boxHeight, startBoxWidth;
		private boolean beingDragged = false;
		private int mouseXOffset = 0, mouseYOffset = 0;
		private DialogObject activeInput;
		private int activeInputNumber = -1;
		private boolean advanced = false;

		public Dialog(String header, int boxWidth, int boxHeight, String text, DialogObject... dialogObjects)
		{
			this.boxWidth = boxWidth;
			this.boxHeight = boxHeight;
			this.startBoxWidth = boxWidth;
			x = (int) (width / 2.0) - (int) (boxWidth / 2.0);
			y = (int) (height / 2.0) - (int) (boxHeight / 2.0);

			if (header != null)
				this.header = header;
			if (text != null)
				this.text = text;

			this.dialogObjects = new ArrayList<DialogObject>();

			// this.dialogObjects = dialogObjects;
			for (DialogObject bO : dialogObjects)
			{
				bO.dialog = this;
				this.dialogObjects.add(bO);
			}

			activateNextInput();
		}

		public void draw()
		{
			rectMode(CORNER);
			fill(0, 50);
			noStroke();
			rect(0, 0, width, height);
			noFill();
			stroke(0);
			strokeWeight(1);
			boxWidth = startBoxWidth + (advanced ? 240 : 0);
			rectMode(CORNER);
			if (beingDragged)
			{
				x = Math.min(width - boxWidth - 1, Math.max(0, mouseX - mouseXOffset));
				y = Math.min(height - 24, Math.max(0, mouseY - mouseYOffset));
			}
			int drawingX = x, drawingY = y;
			rect(drawingX, drawingY, boxWidth, boxHeight);
			noStroke();
			for (int i = 0; i < 4; i++)
			{
				fill(0, 50 - (15 * i));
				rect(drawingX + i, drawingY + i, boxWidth + 2, boxHeight + 2); // shadows
			}
			// colorMode(RGB);
			fill(216, 236, 255);
			drawingX += 1;
			drawingY += 1;
			rect(drawingX, drawingY, boxWidth - 1, 24); // header
			fill(0);
			textAlign(CENTER);
			textSize(16);
			text(header, drawingX + boxWidth / 2, drawingY + 18); // header text
			drawingY += 24;
			fill(255);
			rect(drawingX, drawingY, boxWidth - 1, boxHeight - 24 - 1); // body

			// pushMatrix();
			// translate(x, y);
			for (int i = 0; i < dialogObjects.size(); i++)
			{
				// if (dialogObjects[i] instanceof TextInput)
				// ((TextInput) dialogObjects[i]).draw(boxWidth - 6);
				// else

				dialogObjects.get(i).setPositionAndSize();
				if (dialogObjects.get(i) instanceof Button)
					((Button) dialogObjects.get(i)).updateState();
				dialogObjects.get(i).draw();
			}
			// popMatrix();
		}

		public void activateNextInput()
		{
			int inputNumberOriginal = activeInputNumber;
			DialogObject newActive = null;
			boolean loopFinish = false;
			while (newActive == null && !loopFinish)
			{
				activeInputNumber = (activeInputNumber + 1) % dialogObjects.size();
				newActive = dialogObjects.get(activeInputNumber);
				if (!newActive.selectable)
				{
					newActive = null;
					activeInputNumber++;
					if (activeInputNumber == inputNumberOriginal)
						loopFinish = true;
				}
			}
			if (newActive != null)
				activeInput(newActive);
		}

		public void activeInput(int index)
		{
			activeInputNumber = index;
			activeInput = dialogObjects.get(index);

			for (DialogObject dO : dialogObjects)
				dO.setActive(dO == activeInput);
		}

		public void activeInput(DialogObject dO)
		{
			// boolean found = false;
			for (int i = 0; i < dialogObjects.size(); i++)
				if (dialogObjects.get(i) == dO)
				{
					activeInput(i);
					// println(i);
					// found = true;
					return;
				}
		}

		private DialogObject getDialogObject(String name)
		{
			for (DialogObject dO : dialogObjects)
				if (dO.name.equals(name))
					return dO;
			return null;
		}

		public void deactiveInput(DialogObject dO)
		{
			// activeInputNumber = -1;
			// activeInput = null;
			//
			// for(TextInput tI : textInputs)
			dO.setActive(false);
		}

		public void keyPressed()
		{
			if (keyCode == ESC)
			{
				activeDialog = null;
			}
			else
			{
				if (keyCode == TAB)
				{
					int inputNumber = activeInputNumber;
					deactiveInput(dialogObjects.get(inputNumber));
					activateNextInput();
				}
				for (DialogObject textInput : dialogObjects)
					textInput.keyPressed();
			}
		}

		public void mousePressed()
		{
			if (mouseX > x && mouseX <= x + boxWidth && mouseY > y && mouseY <= y + 24)
			{
				beingDragged = true;
				mouseXOffset = mouseX - x;
				mouseYOffset = mouseY - y;
			}

			if (lastMousePress != -1)
			{
				for (DialogObject tI : dialogObjects)
					tI.mousePressed();
			}
		}

		public void mouseReleased()
		{
			if (beingDragged)
				beingDragged = false;

			for (DialogObject dO : dialogObjects)
				dO.mouseReleased();
		}

		public void close(ButtonResponse response)
		{
			boolean able = true;
			if (response == ButtonResponse.CREATE)
			{
				for (int i = 0; i < dialogObjects.size(); i++)
					if (dialogObjects.get(i) instanceof TextInput)
						if (!((TextInput) dialogObjects.get(i)).isValid(true) && able == true)
							able = false;
				if (able)
				{
					int gridWidth = Integer.parseInt(getDialogObject("widthBox").text);
					int gridHeight = Integer.parseInt(getDialogObject("heightBox").text);
					boolean tint = ((CheckButton) getDialogObject("blueTintCheckButton")).checked;
					createWordsearch("", gridWidth, gridHeight, tint);
				}
			}
			if (able)
				activeDialog = null;
		}

		public void toggleAdvanced()
		{
			advanced = !advanced;
		}
	}

	public abstract class Button extends DialogObject
	{
		protected ButtonState state;

		public void updateState()
		{
			if (mouseOver())
				if (mousePressed)
					if (isInside(mouseXStart, mouseYStart))
						state = ButtonState.PRESSED;
					else
						state = ButtonState.INACTIVE;
				else
					state = ButtonState.MOUSE_OVER;
			else if (mousePressed && isInside(mouseXStart, mouseYStart))
				state = ButtonState.SELECTED;
			else
				state = active ? ButtonState.ACTIVE : ButtonState.INACTIVE;
		}
	}

	public class TextInput extends DialogObject
	{
		String defaultText = "";
		private long lastInteract;
		private int cursorIndex;
		private int fontSize;
		private int validInput = -1;
		// private int lineCount;
		// List<String> lines = new ArrayList<>();
		private int selectedLine;
		// private
		private int lineHeight;

		private TextInputType textInputType;

		public TextInput(String name, TextInputType textInputType, int x, int y, int boxWidth, int boxHeight, String... defaultText)
		{
			this.name = name;

			this.initialBoxHeight = boxHeight;
			this.initialBoxWidth = boxWidth;
			this.initialX = x;
			this.initialY = y;
			// this.lineCount = (int)Math.max(1, lineCount);
			fontSize = (int) ((initialBoxHeight * 2) / ((float) 1 * 3));
			if (defaultText != null && defaultText.length > 0)
				this.defaultText = defaultText[0];
			if (defaultText.length > 1)
				text = defaultText[1];
			this.textInputType = textInputType;
		}

		public void draw()
		{
			// lines
			xPos = x + dialog.x;
			yPos = y + dialog.y;
			int textX = xPos + 4;
			int textY = yPos + boxHeight / 2;
			rectMode(CORNER);
			textAlign(LEFT, CENTER);
			stroke(validInput == -1 ? (!active ? 150 : 0) : validInput == 0 ? color(214, 14, 0) : color(14, 214, 0));
			// strokeWeight(validInput == -1 ? 1 : 2);
			fill(255);
			// noSmooth();
			int thicker = validInput != -1 ? 2 : 1;
			// rect(xPos + (thicker ? 1 : 0), yPos + (thicker ? 1 : 0), boxWidth
			// - (thicker ? 1 : 0), boxHeight - (thicker ? 1 : 0));
			rect(xPos, yPos, boxWidth, boxHeight, thicker);
			// smooth();
			fill(0);
			textSize(fontSize);
			text(text, textX, textY);
			if (active)
			{
				int textLength = (int) textWidth(text);
				int cursorPos = (int) textX + (text.length() > 0 ? (int) (textWidth(text.substring(0, cursorIndex))) : 0);
				if ((System.currentTimeMillis() - lastInteract) % cursorFlashTimeout * 2 <= cursorFlashTimeout)
				{
					stroke(0);
					// strokeWeight(1);
					line(cursorPos, yPos + 3, cursorPos, yPos + boxHeight - 3);
				}
			}
			// else
			// {
			if (text == null || text.equals(""))
			{
				fill(75);
				text(defaultText, textX, textY);
			}
			// }
		}

		public boolean isValid(boolean forced)
		{
			if (!forced && active)
				validInput = -1;
			else
			{
				if (name.equals("widthBox") || name.equals("heightBox"))
				{
					if (text.length() > 0)
					{
						validInput = 1;
					}
					else
					{
						validInput = forced ? 0 : -1;
					}
				}
			}
			return validInput != 0;
		}

		public String getText()
		{
			return text;
		}

		public void keyPressed()
		{
			textSize(20);
			if (active)
			{
				lastInteract = System.currentTimeMillis();
				boolean changed = true;
				// if ((key >= '0' && key <= '9') || (key >= 'a' && key <= 'z')
				// || (key >= 'A' && key <= 'Z') || key == ' ' || key == ',' ||
				// key == '.' || key == '!'
				// || key == '?' || key == '%' || key == '#' || key == '@' ||
				// key == '(' || key == ')' || key == ';' || key == ':' || key
				// == '\"' || key == '\'')
				// {
				if (textInputType.isValid(key))// && (textInputType !=
												// TextInputType.NUMERICAL ||
												// text.length() > 0 || key !=
												// '0'))
				{
					List<Character> chars = new ArrayList<Character>();
					for (char c : text.toCharArray())
						chars.add(c);
					if (textWidth(text + key) < boxWidth - 4)
					{
						// text = text + key;
						chars.add(cursorIndex, key);
						text = "";
						for (char c : chars)
							text += c;
						cursorIndex += 1;
					}

					if (name.equals("widthBox") || name.equals("heightBox") && text.length() > 0)
					{
						int number = Integer.parseInt(text), savedNumber = number;
						number = (int) (Math.max(1, Math.min(30, number)));
						if (savedNumber != number)
							text = Integer.toString(number);
						cursorIndex = text.length();
					}
				}
				// }
				else if (keyCode == BACKSPACE)
				{
					if (cursorIndex > 0)
					{
						char[] chars = text.toCharArray();
						text = "";
						for (int i = 0; i < chars.length; i++)
						{
							if (i != cursorIndex - 1)
							{
								text += chars[i];
							}
						}
						cursorIndex -= 1;
					}
				}
				else if (keyCode == DELETE)
				{
					if (cursorIndex < text.length())
					{
						char[] chars = text.toCharArray();
						text = "";
						for (int i = 0; i < chars.length; i++)
						{
							if (i != cursorIndex)
							{
								text += chars[i];
							}
						}
						// cursorIndex += 1;
					}
				}
				else if (keyCode == LEFT)
				{
					if (cursorIndex > 0)
						cursorIndex -= 1;
				}
				else if (keyCode == RIGHT)
				{
					if (cursorIndex < text.length())
						cursorIndex += 1;
				}
				else if (keyCode == ENTER)
				{
					if (name.equals("heightBox"))
					{
						activeDialog.close(ButtonResponse.CREATE);
						keyCode = -1;
						key = '\0';
					}
					else
					{
						int inputNumber = activeDialog.activeInputNumber;
						activeDialog.deactiveInput(activeDialog.dialogObjects.get(inputNumber));
						activeDialog.activateNextInput();
					}
				}
				else
					changed = false;
			}
		}

		@Override
		public void setActive(boolean isActive)
		{
			super.setActive(isActive);
			isValid(false);
		}

		public boolean isActive()
		{
			return active;
		}

		public void mousePressed()
		{
			if (mouseOver())
			{
				activeDialog.activeInput(this);
				lastInteract = System.currentTimeMillis();
			}
			else
				activeDialog.deactiveInput(this);
		}

		public void mouseReleased()
		{

		}
	}

	public class CheckButton extends Button
	{
		boolean checked = false;
		private int fontSize = 13;
		private int boxSize = 13;

		public CheckButton(String name, int x, int y, String text, boolean checked)
		{
			textSize(fontSize);
			this.name = name;
			this.text = text;

			this.initialBoxHeight = fontSize + boxSize;
			this.initialBoxWidth = (int) (boxSize * 2.5 + textWidth(text));
			this.initialX = x;
			this.initialY = y;
			this.checked = checked;
		}

		public void draw()
		{
			xPos = dialog.x + x;
			yPos = dialog.y + y;

			int borderColour = 0, fill = 0;

			if (state == ButtonState.PRESSED)
			{
				borderColour = color(0, 84, 153);
				fill = color(204, 228, 247);
			}
			else if (state == ButtonState.MOUSE_OVER || state == ButtonState.SELECTED)
			{
				borderColour = color(0, 120, 215);
				fill = color(255);
			}
			else if (state == ButtonState.INACTIVE || state == ButtonState.ACTIVE)
			{
				borderColour = state == ButtonState.INACTIVE ? color(172) : color(0, 120, 215);
				fill = color(255);
			}

			rectMode(CENTER);
			int thickness = state == ButtonState.ACTIVE ? 2 : 1;
			// strokeWeight(state == ButtonState.ACTIVE ? 2 : 1);
			// int offset = state == ButtonState.ACTIVE ? 1 : 0;
			stroke(borderColour);
			fill(fill);
			// strokeWeight(1);
			// noSmooth();
			rect(xPos, yPos, boxSize - 1, boxSize - 1, thickness);
			// rect(xPos + offset, yPos + offset, - offset, - offset);
			// smooth();
			if (checked)
			{
				tint(state == ButtonState.INACTIVE ? color(0) : borderColour);
				imageMode(CENTER);
				image(check, xPos, yPos);
				tint(255);
			}

			fill(0);
			textAlign(LEFT, CENTER);
			textSize(fontSize);
			text(text, xPos + boxSize, yPos - 2);
			rectMode(CORNER);
		}

		@Override
		public void mousePressed()
		{

		}

		@Override
		public void mouseReleased()
		{
			if (state == ButtonState.PRESSED)
				pressed();
		}

		@Override
		public void keyPressed()
		{
			if (active && keyCode == ENTER)
				pressed();
		}

		private void pressed()
		{
			checked = !checked;
		}

		@Override
		public boolean isInside(int x, int y)
		{
			return super.isInside(x + boxSize, y + boxHeight / 2);
		}
	}

	public class TextButton extends Button
	{
		String text;
		int fontSize;

		public TextButton(String name, int x, int y, int boxWidth, int boxHeight, String text)
		{
			this.name = name;
			this.initialBoxHeight = boxHeight;
			this.initialBoxWidth = boxWidth;
			this.initialX = x;
			this.initialY = y;
			fontSize = initialBoxHeight - 8;
			this.text = text;
		}

		public void draw()
		{
			xPos = dialog.x + x;
			yPos = dialog.y + y;

			int borderColour = 0, fill = 0;

			if (state == ButtonState.PRESSED)
			{
				borderColour = color(0, 84, 153);
				fill = color(204, 228, 247);
			}
			else if (state == ButtonState.MOUSE_OVER || state == ButtonState.SELECTED)
			{
				borderColour = color(0, 120, 215);
				fill = color(229, 241, 251);
			}
			else if (state == ButtonState.INACTIVE || state == ButtonState.ACTIVE)
			{
				borderColour = state == ButtonState.INACTIVE ? color(172) : color(0, 120, 215);
				fill = color(225);
			}

			stroke(borderColour);
			int thickness = state == ButtonState.ACTIVE ? 2 : 1;
			// strokeWeight(state == ButtonState.ACTIVE ? 2 : 1);
			// int offset = state == ButtonState.ACTIVE ? 1 : 0;
			fill(fill);
			// noSmooth();
			rect(xPos, yPos, boxWidth, boxHeight, thickness);
			// smooth();
			fill(0);
			textAlign(CENTER, CENTER);
			textSize(fontSize);
			text(text, xPos + boxWidth / 2, yPos + boxHeight / 2 - 2);
			// print("hey");
		}

		@Override
		public void mousePressed()
		{

		}

		@Override
		public void mouseReleased()
		{
			if (state == ButtonState.PRESSED)
			{
				pressed();
			}
		}

		@Override
		public void keyPressed()
		{
			if (keyCode == ENTER && isActive())
				pressed();
		}

		private void pressed()
		{
			dialog.close(name.equals("create") ? ButtonResponse.CREATE : ButtonResponse.CANCEL);
		}
	}

	void changeAppIcon(PImage img)
	{
		final PGraphics pg = createGraphics(32, 32, JAVA2D);

		pg.beginDraw();
		pg.image(img, 0, 0, 32, 32);
		pg.endDraw();

		frame.setIconImage(pg.image);
	}

	public class WordBox
	{
		int xPos, yPos;
	}

	public class AddNewWord extends WordBox
	{
		boolean active = false;
		TextInputType textInputType = TextInputType.SPELLINGS;
		String text = "";
		int cursorIndex = 0, textSize;
		long lastInteract;

		public AddNewWord()
		{
			textSize = wordBankTextSize;
		}

		public void draw()
		{
			active = active && activeDialog == null;
			rectMode(CORNER);
			textAlign(CENTER, CENTER);
			stroke(0);
			strokeWeight(1);
			fill(204, 0, 0, 50);
			rrect(xPos, yPos, wordBankBoxWidth, wordBankBoxHeight, 8);

			fill(0);
			textFont(lSans);
			calculateFontSize();
			textSize(textSize);

			int textX = (int) (xPos + wordBankBoxWidth / 2f), textY = (int) (yPos + wordBankBoxHeight / 2f) - 3;
			text(text, textX, textY);
			if (active)
			{
				int textLength = (int) textWidth(text);
				int cursorPos = (int) textX - (int) (textLength / 2f) + (text.length() > 0 ? (int) (textWidth(text.substring(0, cursorIndex))) : 0);
				if ((System.currentTimeMillis() - lastInteract) % cursorFlashTimeout * 2 <= cursorFlashTimeout)
				{
					fill(0);
					line(cursorPos, textY - (int) (textSize * 1.1f / 2f), cursorPos, textY + (int) (textSize * 1.8f / 2f));
				}
			}
			else if (text == null || text.equals(""))
			{
				fill(75);
				textSize(wordBankTextSize);
				text("Add word", textX, textY);
			}
		}

		public void calculateFontSize()
		{
			this.textSize = wordBankTextSize;
			textFont(lSans);
			textSize(textSize);
			if (textWidth(text) > Main.wordBankBoxWidth - 4)
				textSize = Math.max(3, (int) ((Main.wordBankBoxWidth - 4f) * textSize / textWidth(text)));
		}

		public void clear()
		{
			text = "";
			cursorIndex = 0;
			lastInteract = System.currentTimeMillis();
		}

		public boolean mouseOver()
		{
			return mouseX >= xPos && mouseX < xPos + wordBankBoxWidth && mouseY >= yPos && mouseY < yPos + wordBankBoxHeight;
		}

		public void mousePressed()
		{
			active = mouseOver();
			if (active)
			{
				lastInteract = System.currentTimeMillis();
			}
		}

		public void keyPressed()
		{
			textSize(textSize);
			if (active)
			{
				lastInteract = System.currentTimeMillis();
				boolean changed = true;
				// if ((key >= '0' && key <= '9') || (key >= 'a' && key <= 'z')
				// || (key >= 'A' && key <= 'Z') || key == ' ' || key == ',' ||
				// key == '.' || key == '!'
				// || key == '?' || key == '%' || key == '#' || key == '@' ||
				// key == '(' || key == ')' || key == ';' || key == ':' || key
				// == '\"' || key == '\'')
				// {

				// }
				if (keyCode == BACKSPACE)
				{
					if (cursorIndex > 0)
					{
						char[] chars = text.toCharArray();
						text = "";
						for (int i = 0; i < chars.length; i++)
						{
							if (i != cursorIndex - 1)
							{
								text += chars[i];
							}
						}
						cursorIndex -= 1;
					}
				}
				else if (keyCode == DELETE)
				{
					if (cursorIndex < text.length())
					{
						char[] chars = text.toCharArray();
						text = "";
						for (int i = 0; i < chars.length; i++)
						{
							if (i != cursorIndex)
							{
								text += chars[i];
							}
						}
						// cursorIndex += 1;
					}
				}
				else if (keyCode == LEFT)
				{
					if (cursorIndex > 0)
						cursorIndex -= 1;
				}
				else if (keyCode == RIGHT)
				{
					if (cursorIndex < text.length())
						cursorIndex += 1;
				}
				else if (keyCode == ENTER || keyCode == TAB || key == ' ')
				{
					if (wordBoard.wordBank.addWord(text))
					{
						clear();
						updateCursor();
						calculateFontSize();
					}
				}
				else if (keyCode == ESC)
				{
					clear();
					calculateFontSize();
					active = false;
				}
				else if (textInputType.isValid(key))
				{
					key = Character.toLowerCase(key);
					List<Character> chars = new ArrayList<Character>();
					for (char c : text.toCharArray())
						chars.add(c);
					// if (textWidth(text + key) < wordBankBoxWidth - 4)
					// {
					// text = text + key;
					chars.add(cursorIndex, key);
					text = "";
					for (char c : chars)
						text += c;
					cursorIndex += 1;
					calculateFontSize();
					// }
				}
				else
					changed = false;
			}
			else
			{
				if (keyCode == ENTER)
				{
					active = true;
				}
			}
		}
	}

	public class WordBankWord extends WordBox
	{
		int index;
		boolean mouseOver = false, used = false, eligible = true;
		WordError error = WordError.NONE;
		String errorMessage = null;
		boolean deleted = false;
		// PImage resizedBackground;

		float xAddition;
		int framesMouseOver;

		public WordBankWord(int index)
		{
			this.index = index;
			// this.xPos = xPos;
			// this.yPos = yPos;
			calculateFontSize();
			// float denominator = (float) (Math.max(words.size() - 1, 1));
			// float increment = Main.wordBankBoxheight / denominator;
			// this.textSize = (int) (Math.min(increment * 2 / 3f, 28));
			// increment -= textSize / denominator;
			// (int) (topY + increment * (index) + textSize);
			// try
			// {
			// resizedBackground = (PImage) textBackground.clone();
			// resizedBackground.resize((int) (textWidth(words.get(index)) +
			// 32), textSize + 8);
			// }
			// catch (CloneNotSupportedException e)
			// {
			// e.printStackTrace();
			// }
		}

		public void draw()
		{
			boolean newMouseOver = activeDialog == null && mouseOver();
			used = used();
			if (mouseOver && !newMouseOver)
				mouseLeave();
			else if (!mouseOver && newMouseOver)
				mouseEnter();
			mouseOver = newMouseOver;
			String word = wordBoard.getWord(index, false);

			if (error != WordError.TOO_LONG && word.length() > Math.max(wordBoard.columns, wordBoard.rows))
			{
				error = WordError.TOO_LONG;
			}
			// else if(error == WordError.NONE && !eligible)
			// {
			// error = WordError.INELIGIBLE;
			// }

			if (error != WordError.NONE)
			{
				imageMode(CORNER);
				image(errorIcon, xPos - errorIcon.width - 2, yPos + (int) (wordBankBoxHeight / 2f - errorIcon.height / 2f));
			}

			if (mouseOver)
				framesMouseOver++;

			if (mouseOver)
			{
				if (used)
				{
					Word wordObject = wordBoard.getWordObject(wordBoard.getWord(index, true));
					// List<Coordinate> coords =
					// wordBoard.getLetterCoordinates(wordObject);
					wordBoard.drawWordRectangle(wordObject.x, wordObject.y, wordObject.x2, wordObject.y2);
				}
			}

			// if (wordBoard.wordIndex == index)
			// {
			// image(resizedBackground, xPos - 16, yPos - 16 - 4);
			// }

			boolean selected = wordBoard.wordIndex == index || error != WordError.NONE;
			stroke(selected ? color(255, 0, 0) : 0);
			fill(255);
			strokeWeight(selected ? 2 : 1);
			rrect(xPos, yPos, Main.wordBankBoxWidth, Main.wordBankBoxHeight, 8);
			fill(used ? 204 : 0);
			textAlign(CENTER, CENTER);
			// textFont(which);
			calculateFontSize();
			textSize(wordBankTextSize);
			text(words.get(index), xPos + Main.wordBankBoxWidth / 2f, yPos + Main.wordBankBoxHeight / 2f - 3);
			if (mouseOver)
			{
				imageMode(CORNER);
				image(deleteButton, xPos + wordBankBoxWidth - deleteButton.width + 1, yPos);
				if (error != WordError.NONE)
				{
					int sizeOfText = 12;
					textSize(sizeOfText);
					fill(255, 0, 0);
					textAlign(CENTER, BOTTOM);
					text(error.errorMessage, xPos + (int) (wordBankBoxWidth / 2f), yPos + wordBankBoxHeight + sizeOfText + 4);
				}
			}
		}

		public boolean updateEligible()
		{
			if (used())
				return true;
			String word = wordBoard.getWord(index, false);
			boolean eligible;
			int ineligibleCount = 0;
			List<CellEligibility> allEligibilities = new ArrayList<>();
			for (int i = 0; i <= 3; i++)
			{
				Map<Coordinate, CellEligibility> cellEligibilities = wordBoard.getEligiblePositions(word, i - 1);

				allEligibilities.addAll(cellEligibilities.values());
			}
			eligible = allEligibilities.contains(CellEligibility.ELIGIBLE) || allEligibilities.contains(CellEligibility.PREFERED);
			// println(" --> " + eligible + "\n");
			if (eligible && error == WordError.INELIGIBLE)
				error = WordError.NONE;
			else if (!eligible && error == WordError.NONE)
				error = WordError.INELIGIBLE;
			return eligible;
		}

		public void calculateFontSize()
		{
			textFont(lSans);
			textSize(wordBankTextSize);
			if (textWidth() > Main.wordBankBoxWidth - 4)
				wordBankTextSize = Math.max(3, (int) ((Main.wordBankBoxWidth - 4f) * wordBankTextSize / textWidth()));
		}

		private float textWidth()
		{
			textSize(wordBankTextSize);
			return Main.this.textWidth(words.get(index));
		}

		public boolean used()
		{
			return wordBoard.containsWord(wordBoard.getWord(index, true));
		}

		public boolean mouseOver()
		{
			return mouseX >= xPos && mouseX < xPos + Main.wordBankBoxWidth && mouseY >= yPos && mouseY < yPos + Main.wordBankBoxHeight;
		}

		public void mouseEnter()
		{
			framesMouseOver = 0;
		}

		public void mouseLeave()
		{
			framesMouseOver = -1;
		}

		public boolean mousePressed()
		{
			if (mouseOver() && mouseX > xPos + wordBankBoxWidth - deleteButton.width && mouseY < yPos + deleteButton.height)
			{
				return delete();
			}
			else if (!used && error == WordError.NONE)
			{
				wordBoard.wordIndex = (wordBoard.wordIndex == index ? -1 : index);
				wordBoard.characterOffset = 0;
				wordBoard.updateEligiblePositions();
			}
			return false;
		}

		private String word()
		{
			return words.get(index);
		}

		private boolean delete()
		{
			boolean permanent = true;
			if (used)
			{
				// println(wordBoard.wordObjects);
				Word word = wordBoard.getWordObject(words.get(index));
				wordBoard.wordObjects.remove(word);
				// word.word = null;
				wordBoard.wordBank.updateWords();
				// println("testing");
				used = false;
				permanent = false;
			}
			else
			{
				deleteIndex(index);
			}
			wordBoard.updateEligiblePositions();
			return permanent;
		}
	}

	public class WordBank
	{
		private List<WordBankWord> wordBankWords = new ArrayList<>();
		private AddNewWord addNewWord;
		private int topX, topY, boxWidth, boxHeight, bankTopY = 112, buttonWidth = 140, buttonHeight = 32, seperation = 17, rows, columns;
		boolean mousePressed = false;
		private List<WordBankWord> deletedWords;
		private int longestWord = -1;

		public WordBank(int topX, int topY, int boxHeight)
		{
			this.topX = topX;
			this.topY = topY;
			this.boxWidth = width - topX - topY;
			this.boxHeight = boxHeight;
			Main.wordBankTextSize = (int) (buttonHeight * 1 / 2f);
			calculateCellQuantities();
			for (int i = 0; i < words.size(); i++)
			{
				wordBankWords.add(new WordBankWord(i));
				calculatePosition(i, true);
			}
			addNewWord = new AddNewWord();
			calculatePosition(words.size(), true);
		}

		public void calculateCellQuantities()
		{
			rows = (int) (((boxHeight + 64) - bankTopY + 18) / (buttonHeight + 18f));
			columns = (int) Math.ceil((words.size() + 1) / (float) rows);
			Main.wordBankBoxWidth = buttonWidth;
			Main.wordBankBoxHeight = buttonHeight;
			Main.wordBankTextSize = (int) (buttonHeight * 1 / 2f);
		}

		public Coordinate calculatePosition(int i, boolean apply)
		{
			int xPos = (int) ((topX + boxWidth / 2f) + (int) (i / (float) rows) * (buttonWidth + seperation) - (columns * (buttonWidth + seperation) - seperation) / 2);
			int yPos = topY + bankTopY + (i % rows) * (buttonHeight + seperation);
			if (apply)
			{
				WordBox wordBox = i < words.size() ? wordBankWords.get(i) : addNewWord;
				wordBox.xPos = xPos;
				wordBox.yPos = yPos;
			}
			// println(i + ") " + xPos + ", " + yPos);
			return new Coordinate(xPos, yPos);
		}

		public void draw()
		{
			// if(!this.mousePressed && Main.this.mousePressed)
			// mousePressed();
			textFont(lSans);
			textSize(70);
			fill(0);
			textAlign(CENTER, CENTER);
			text("Word Bank", topX + boxWidth / 2f, topY + 35);
			textSize(14);
			fill(100);
			text(longestWord != -1 ? "Longest word: " + wordBoard.getWord(longestWord, true) + " (" + wordBoard.getWord(longestWord, false).length() + ")" : "", topX
					+ boxWidth / 2f, topY + 88);
			for (WordBankWord wordBankWord : wordBankWords)
			{
				wordBankWord.draw();
			}
			addNewWord.draw();
		}

		public void delete(int index)
		{
			for (int i = index + 1; i < wordBankWords.size(); i++)
			{
				wordBankWords.get(i).index = i - 1;
			}
			wordBankTextSize = 2;
		}

		public void pasteWords()
		{
			cursor(WAIT);
			try
			{
				String data = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
				if (data != null)
				{
					List<String> spellingsToAdd = new ArrayList<>();
					data = data.replaceAll("(\\n)+", ",").replaceAll("(\\s)+", ",").replace(".", ",");
					String[] spellings = data.split(",");
					for (String spelling : spellings)
					{
						wordBoard.wordBank.addWord(spelling);
					}
				}
			}
			catch (HeadlessException | UnsupportedFlavorException | IOException e)
			{
				// do nothing
			}
			cursor(Cursor.DEFAULT_CURSOR);
		}

		public void copyWords()
		{
			StringSelection stringSelection = new StringSelection(wordBoard.returnWords());
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
		}

		public boolean addWord(String word)
		{
			String[] components = word.split("%i%");
			if (components.length > 0)
				word = removeIllegalCharacters(components[0].toLowerCase().trim(), TextInputType.SPELLINGS);
			else
				return false;

			if (word.length() > 1 && (columns < 3 || (words.size() < (rows * columns) - 1)))
			{
				if (!words.contains(word))
				{
					words.add(word);
					int index = words.size() - 1;
					wordBankWords.add(new WordBankWord(index));
					calculateCellQuantities();
					for (int i = 0; i <= words.size(); i++)
					{
						calculatePosition(i, true);
					}
				}

				if (!wordBoard.containsWord(word) && components.length > 1)
				{
					// println(components[1]);
					String[] smallParts = components[1].split("\\|");
					for (String string : smallParts)
					{
						println(word + " " + string);
					}
					int x = Integer.parseInt(smallParts[0]);
					int y = Integer.parseInt(smallParts[1]);
					int orientation = Integer.parseInt(smallParts[2]);
					wordBoard.attemptToPlaceWord(word, x, y, orientation);
				}
				else
					updateWords();
				return true;
			}
			return false;
		}

		public void updateWords()
		{
			int length = 0;
			longestWord = -1;
			for (WordBankWord wordBankWord : wordBankWords)
			{
				wordBankWord.updateEligible();
				String word = wordBoard.getWord(wordBankWord.index, false);
				if (word.length() > length)
				{
					if (longestWord != -1)
						println(word + " is longer than " + wordBoard.getWord(longestWord, false));
					longestWord = wordBankWord.index;
					length = word.length();
				}
			}
		}

		public void mousePressed()
		{
			deletedWords = new ArrayList<>();
			for (WordBankWord wordBankWord : wordBankWords)
			{
				if (wordBankWord.mouseOver())
				{
					if (wordBankWord.mousePressed())
						deletedWords.add(wordBankWord);
				}
			}

			addNewWord.mousePressed();
			if (deletedWords.size() > 0)
			{
				wordBankWords.removeAll(deletedWords);
				calculateCellQuantities();
				for (int i = 0; i <= wordBankWords.size(); i++)
				{
					calculatePosition(i, true);
				}
				updateWords();
			}
		}

		public void keyPressed()
		{
			addNewWord.keyPressed();
		}
	}

	public class NameInputBox
	{
		boolean active = false;
		TextInputType textInputType = TextInputType.TITLE;
		String text = "";
		int cursorIndex = 0, textSize, originalTextSize;
		int xPos, yPos, boxWidth, boxHeight;
		long lastInteract;

		public NameInputBox(String text, int xPos, int yPos, int boxWidth, int boxHeight)
		{
			textSize = 48;
			this.text = text;
			originalTextSize = textSize;
			this.boxWidth = boxWidth;
			this.boxHeight = boxHeight;
			this.xPos = xPos - boxWidth / 2;
			this.yPos = yPos - boxHeight / 2;
		}

		public void draw()
		{
			active = active && activeDialog == null;
			rectMode(CORNER);
			textAlign(CENTER, CENTER);
			textFont(lSans);
			textSize(textSize);

			int textX = (int) (xPos + boxWidth / 2f), textY = (int) (yPos + boxHeight / 2f) - 3;
			if (mouseOver())
			{
				stroke(0);
				strokeWeight(1);
				float textWidth = Math.max(64, textWidth(getText()) + 6), textHeight = textSize + 3;
				fill(255);
				rrect(textX - textWidth / 2, textY + 5 - textHeight / 2, textWidth, textHeight, 2);
			}
			fill(0);
			if (active)
			{
				int textLength = (int) textWidth(text);
				int cursorPos = (int) textX - (int) (textLength / 2f) + (text.length() > 0 ? (int) (textWidth(text.substring(0, cursorIndex))) : 0);
				if ((System.currentTimeMillis() - lastInteract) % cursorFlashTimeout * 2 <= cursorFlashTimeout)
				{
					fill(0);
					line(cursorPos, textY - (int) (textSize * 1.1f / 2f), cursorPos, textY + (int) (textSize * 1.8f / 2f));
				}
			}
			text(getText(), textX, textY);
		}

		public void calculateFontSize()
		{
			this.textSize = originalTextSize;
			textFont(lSans);
			textSize(textSize);
			float textWidth = textWidth(getText());
			if (textWidth > boxWidth - 4)
				textSize = Math.max(3, (int) ((boxWidth - 4f) * textSize / textWidth));
		}

		public String getName()
		{
			return text.trim();
		}

		public String getText()
		{
			if (!active && (text.length() == 0 || text == null))
			{
				return "Name";
			}
			else
			{
				return text;
			}
		}

		public boolean keyPressed()
		{
			textSize(textSize);
			if (active)
			{
				lastInteract = System.currentTimeMillis();
				boolean changed = true;
				if (keyCode == BACKSPACE)
				{
					if (cursorIndex > 0)
					{
						char[] chars = text.toCharArray();
						text = "";
						for (int i = 0; i < chars.length; i++)
						{
							if (i != cursorIndex - 1)
							{
								text += chars[i];
							}
						}
						cursorIndex -= 1;
					}
				}
				else if (keyCode == DELETE)
				{
					if (cursorIndex < text.length())
					{
						char[] chars = text.toCharArray();
						text = "";
						for (int i = 0; i < chars.length; i++)
						{
							if (i != cursorIndex)
							{
								text += chars[i];
							}
						}
					}
				}
				else if (keyCode == LEFT)
				{
					if (cursorIndex > 0)
						cursorIndex -= 1;
				}
				else if (keyCode == RIGHT)
				{
					if (cursorIndex < text.length())
						cursorIndex += 1;
				}
				else if (keyCode == ENTER)
				{
					active = false;
				}
				else if (keyCode == ESC)
				{
					active = false;
				}
				else if (textInputType.isValid(key) && (key != ' ' || (cursorIndex != 0)))
				{
					List<Character> chars = new ArrayList<Character>();
					for (char c : text.toCharArray())
						chars.add(c);
					chars.add(cursorIndex, key);
					text = "";
					for (char c : chars)
						text += c;
					cursorIndex += 1;
				}
				else
					changed = false;
				calculateFontSize();
				return true;
			}
			else
			{
				return false;
			}
		}

		public void mousePressed()
		{
			active = mouseOver();
			if (active)
			{
				lastInteract = System.currentTimeMillis();
			}
		}

		public boolean mouseOver()
		{
			return mouseX > xPos && mouseX < xPos + boxWidth && mouseY > yPos && mouseY < yPos + boxHeight;
		}
	}
}
